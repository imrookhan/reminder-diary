import React, { useState } from "react";
import "./report.css";
import { FaAngleRight } from "react-icons/fa";
function Report() {
  const [weeklyDropdown, setWeeklyDropdown] = useState(false);
  const [monthlyDropdown, setMonthlyDropdown] = useState(false);
  const [yearlyDropdown, setYearlyDropdown] = useState(false);
  const [customDays, setCustomDays] = useState(false);

  return (
    <>
      <div className="container mt-1">
        <div className="card card-1">
          <div className="text">
            <p>Routine Report August 2021</p>
          </div>
          <div className="next__div">
            <div
              className="Weekly d-flex px-3 cursor-pointer"
              onClick={() => setWeeklyDropdown(!weeklyDropdown)}
            >
              <span>
                <FaAngleRight color="black" />
              </span>
              <p>Weekly routine</p>
            </div>
            {weeklyDropdown ? (
              <div className="weekly__dropdown px-2">
                <div className="weekly__dropdown__item">
                  <p>Week 1</p>
                </div>
                <div className="weekly__dropdown__item">
                  <p>Week 2</p>
                </div>
                <div className="weekly__dropdown__item">
                  <p>Week 3</p>
                </div>
              </div>
            ) : null}
            <div
              className="Monthly d-flex px-3"
              onClick={() => setMonthlyDropdown(!monthlyDropdown)}
            >
              <span>
                <FaAngleRight color="black" />
              </span>
              <p>Monthly routine</p>
            </div>
            {monthlyDropdown ? (
              <div className="monthly__dropdown px-2">
                <div className="monthly__dropdown__item">
                  <p>Month 1</p>
                </div>
                <div className="monthly__dropdown__item">
                  <p>Month 2</p>
                </div>
                <div className="monthly__dropdown__item">
                  <p>Month 3</p>
                </div>
              </div>
            ) : null}
            <div
              className="Yearly d-flex px-3"
              onClick={() => setYearlyDropdown(!yearlyDropdown)}
            >
              <span>
                <FaAngleRight color="black" />
              </span>
              <p>Yearly routine</p>
            </div>
            {yearlyDropdown ? (
              <div className="yearly__dropdown px-2">
                <div className="yearly__dropdown__item">
                  <p>Year 1</p>
                </div>
                <div className="yearly__dropdown__item">
                  <p>Year 2</p>
                </div>
                <div className="yearly__dropdown__item">
                  <p>Year 3</p>
                </div>
              </div>
            ) : null}
            <div
              className="Custom d-flex px-3"
              onClick={() => setCustomDays(!customDays)}
            >
              <span>
                <FaAngleRight color="black" />
              </span>
              <p>Custom days</p>
            </div>
            {customDays ? (
              <div className="custom__dropdown px-2">
                <div className="custom__dropdown__item">
                  <p>Custom 1</p>
                </div>
                <div className="custom__dropdown__item">
                  <p>Custom 2</p>
                </div>
                <div className="custom__dropdown__item">
                  <p>Custom 3</p>
                </div>
              </div>
            ) : null}
          </div>
        </div>
      </div>
    </>
  );
}

export default Report;
